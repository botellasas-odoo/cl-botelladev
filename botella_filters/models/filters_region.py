# -*- coding: utf-8 -*-
from odoo import models, fields, api

class FiltersRegion(models.Model):
    _name = 'res.country.region'

    name = fields.Char('Region', required=False)
    country_id = fields.Many2one('res.country', 'Pais')
