# -*- coding: utf-8 -*-
from odoo import models, fields, api


class FiltersBrand(models.Model):
    _name = 'product.brand'
    _inherit = 'product.brand'
    
    bodega_id = fields.Many2one('product.bodega','Bodega')
    manufacter_id = fields.Many2one('product.manufacter','Fabricante/Grupo')

    