{
    'name' : 'Botella Website',
    'summary': 'Módulo con adaptaciones web',
    'author': 'Gabriela Rivero',
    'depends': ['website_sale',
                'alan_customize'
               ],
    'application': False,
    'data' : ['views/templates.xml',
              'views/assets.xml',
             ]
}
