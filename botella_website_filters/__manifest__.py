# -*- coding: utf-8 -*-
{
    'name' : 'Botella Website Filters',
    'summary': 'Módulo con adaptaciones web/filtros pais, region, bodega, fabricante',
    'author': 'Gabriela Rivero',
    'depends': ['website_sale',
                'alan_customize',
                'botella_filters',
               ],
    'application': False,
    'data' : [
        'views/templates.xml',
        'views/assets.xml',
        ],
    'qweb': [
    ],
}
