# -*- coding: utf-8 -*-
{
    'name': 'Menu General',
    'description': 'Módulo que coloca iconos a las aplicaciones',
    'author': 'Bazan Daniela Romina',
    'depends': [
        'calendar',
        'contacts',
        'alan_customize',
        'mail',
        'base',
        'crm',
        'mass_mailing',
        'marketing_automation',
        # 'hr_attendance',
        'account',
        'purchase',
        'documents',
        'hr',
        'im_livechat',
        'website_sale',
        'botella_menu',
        'stock_enterprise',
        'utm',
        'link_tracker'
    ],
    'application': True,
    'data':[
        'security/res_group.xml',
        'views/menu_general_view.xml',
        'views/template.xml'
    ]
}
