{
    'name' : 'Productos',
    'description': 'Menu para dar de alta productos',
    'author': 'Romina Bazan',
    'depends': ['base',
                'load_variants_products',
                'product',
                'botella_product',
                'import_product_image',
                'website_sale',
                'alan_customize'
               ],
    'application': True,
    'data' : ['views/menu_view.xml']
}
