#!/usr/bin/python

import sys
import xmlrpc.client
import ssl
import csv

username = 'admin' #the user
pwd = 'bote66lla' #the user
dbname = 'botelladev_prod'    #the database

def create_location(pos1, loc_stock_id):
    loc_id = sock.execute(dbname,uid,pwd,'stock.location','create', {
                        'name': pos1,
                        'location_id': loc_stock_id[0],
                        'usage': 'internal',
                        'barcode': 'PANTO-%s'%(pos1)})
    return [loc_id]

def create_quant(product_id, botellasas_id, loc1, q1):
    quant_id = sock.execute(dbname,uid,pwd,'stock.quant','create', {
        'product_id': product_id[0],
        'company_id': botellasas_id[0],
        'location_id': loc1[0],
        'quantity': q1,
    })
    return quant_id

gcontext = ssl._create_unverified_context()

# Get the uid
sock_common = xmlrpc.client.ServerProxy ('http://localhost:8069/xmlrpc/common',context=gcontext)
uid = sock_common.login(dbname, username, pwd)

#replace localhost with the address of the server
sock = xmlrpc.client.ServerProxy('http://localhost:8069/xmlrpc/object',context=gcontext)

f = open('inventario.csv','rt')
csv_reader = csv.reader(f,delimiter=';')
location_ids = sock.execute(dbname,uid,pwd,'stock.location','search',[('usage','=','internal')])
loc_stock_id = sock.execute(dbname,uid,pwd,'stock.location','search',[('usage','=','internal'),('name','=','Stock')])
botellasas_id = sock.execute(dbname,uid,pwd,'res.company','search',[('name','=','Botella SAS')])

print (loc_stock_id)
#1/0
print (location_ids)
count = 0
for line in csv_reader:
    #print (line)
    if count == 0:
        count += 1
        continue
    code = line[0]
    product = line[1]
    uni_caja = line[2]
    uni_caja2 = line[3]
    venta_min = line[4]
    categ = line[5]
    subcateg = line[6]
    barcode = line[7]
    box_barcode = line[8]
    rem1 = line[9]
    rem2 = line[10]
    rem3 = line[11]
    rem4 = line[12]
    rem_total = line[13]
    diff = line[14]
    conteo_panto = line[15]
    peso_caja = line[16]
    alto_caja = line[17]
    ancho_caja = line[18]
    largo_caja = line[19]
    peso_uni = line[20]
    alto_uni = line[21]
    ancho_uni = line[22]
    largo_uni = line[23]
    pos1 = line[24]
    q1 = int(line[25])
    pos2 = line[26]
    q2 = int(line[27])
    pos3 = line[28]
    q3 = int(line[29])
    pos4 = line[30]
    q4 = int(line[31])
    pos5 = line[32]
    q5 = int(line[33])
    pos6 = line[34]
    q6 = int(line[35])
    pos7 = line[36]
    q7 = int(line[37])
    pos8 = line[38]
    q8 = int(line[39])
    pos9 = line[40]
    q9 = int(line[41])
    costo = line[42]
    land_sin_imp = line[43]
    land_con_imp = line[44]
    precio_publi = line[45]
    precio_seg = line[46]

    #1/0

    if code:
        found = True
        product_id = sock.execute(dbname,uid,pwd,'product.product','search',[('default_code','=',code)])
        if not product_id:
            product_id = sock.execute(dbname,uid,pwd,'product.product','search',[('name','=',product)])
            #print ('product_id', product_id)
            if not product_id:
                product_id = sock.execute(dbname,uid,pwd,'product.product','search',[('barcode','=',barcode)])
                if not product_id:
                    print ('no encontrado %s'%(product))
                    found = False
        if found:
            print (q1, q2,q3,q4,q5,q6,q7,q8,q9)
            if q1 != 0:
                loc1 = sock.execute(dbname,uid,pwd,'stock.location','search',[('name','=',pos1)])
                if not loc1:
                    loc1 = create_location(pos1, loc_stock_id)
                quant = create_quant(product_id, botellasas_id, loc1, q1)
                print ('quant', quant)
            if q2 != 0:
                loc2 = sock.execute(dbname,uid,pwd,'stock.location','search',[('name','=',pos2)])
                if not loc2:
                    loc2 = create_location(pos2, loc_stock_id)
                quant = create_quant(product_id, botellasas_id, loc2, q2)
                print ('quant', quant)
            if q3 != 0:
                loc3 = sock.execute(dbname,uid,pwd,'stock.location','search',[('name','=',pos3)])
                if not loc3:
                    loc3 = create_location(pos3, loc_stock_id)
                quant = create_quant(product_id, botellasas_id, loc3, q3)
                print ('quant', quant)
            if q4 != 0:
                loc4 = sock.execute(dbname,uid,pwd,'stock.location','search',[('name','=',pos4)])
                if not loc4:
                    loc4 = create_location(pos4, loc_stock_id)
                quant = create_quant(product_id, botellasas_id, loc4, q4)
                print ('quant', quant)
            if q5 != 0:
                loc5 = sock.execute(dbname,uid,pwd,'stock.location','search',[('name','=',pos5)])
                if not loc5:
                    loc5 = create_location(pos5, loc_stock_id)
                quant = create_quant(product_id, botellasas_id, loc5, q5)
                print ('quant', quant)
            if q6 != 0:
                loc6 = sock.execute(dbname,uid,pwd,'stock.location','search',[('name','=',pos6)])
                if not loc6:
                    loc6 = create_location(pos6, loc_stock_id)
                quant = create_quant(product_id, botellasas_id, loc6, q6)
                print ('quant', quant)
            if q7 != 0:
                loc7 = sock.execute(dbname,uid,pwd,'stock.location','search',[('name','=',pos7)])
                if not loc7:
                    loc7 = create_location(pos7, loc_stock_id)
                quant = create_quant(product_id, botellasas_id, loc7, q7)
                print ('quant', quant)
            if q8 != 0:
                loc8 = sock.execute(dbname,uid,pwd,'stock.location','search',[('name','=',pos8)])
                if not loc8:
                    loc8 = create_location(pos8, loc_stock_id)
                quant = create_quant(product_id, botellasas_id, loc8, q8)
                print ('quant', quant)
            if q9 != 0:
                loc9 = sock.execute(dbname,uid,pwd,'stock.location','search',[('name','=',pos9)])
                if not loc9:
                    loc9 = create_location(pos9, loc_stock_id)
                quant = create_quant(product_id, botellasas_id, loc9, q9)
                print ('quant', quant)
        #1/0

        # else:
        #     quant_id = sock.execute(dbname,uid,pwd,'stock.quant','search',[('product_id','=',product_id[0]),('location_id','in',location_id)])

        #     print (quant_id)
        #     quant_data = sock.execute(dbname,uid,pwd,'stock.quant','read',quant_id)
        #     print (quant_data)




# location_id = sock.execute(dbname,uid,pwd,'stock.location','search',[('usage','=','internal')])
# product_id = sock.execute(dbname,uid,pwd,'product.product','search',[('default_code','=','PROD_STOCK')])

# quant_id = sock.execute(dbname,uid,pwd,'stock.quant','search',[('product_id','=',product_id[0]),('location_id','=',location_id[0])])
# if quant_id:
# 	quant_data = sock.execute(dbname,uid,pwd,'stock.quant','read',quant_id)
# 	print quant_data
# 	vals_update = {
# 		'quantity': 555
# 		}
# 	return_id = sock.execute(dbname,uid,pwd,'stock.quant','write',quant_id,vals_update)
# 	print return_id
