{
    'name' : 'Botella Product',
    'description': ' Agrega campos en productos',
    'author': 'Romina Bazan',
    'depends': ['product', 'alan_customize'],
    'data' : ['views/products_view.xml',
              'views/products_packaging_view.xml',
              'views/products_brand.xml',
              'views/products_variety.xml',
              'security/ir.model.access.csv',
              'security/res_group.xml',
              'data/data.xml',
              'data/uom_data.xml',
             ]
}
