# -*- coding: utf-8 -*-
from odoo import models, fields, api


class ProductFormat(models.Model):
    _name = 'product.format'
    name = fields.Char('Descripcion', required=False)
